@echo off
cd %cd%

SET wget_file=wget.exe
SET wget_url=https://eternallybored.org/misc/wget/1.20/64/%wget_file%

SET zip_file=7za.exe
SET zip_url=https://github.com/develar/7zip-bin/raw/master/win/x64/%zip_file%

SET jdk8_folder=jdk8u202-b08
SET jdk8_file=OpenJDK8U-jdk_x64_windows_hotspot_8u202b08.zip
SET jdk8_wildcard_file=OpenJDK8U-jdk_x64_windows_hotspot_*.zip
SET jdk8_url=https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/%jdk8_folder%/%jdk8_file%

SET android_sdk_file=sdk-tools-windows-4333796.zip
SET android_sdk_wildcard_file=sdk-tools-windows-*.zip
SET android_sdk_url=https://dl.google.com/android/repository/%android_sdk_file%
SET android_sdk_folder=android-sdk
SET tools_folder=tools

SET adhell3_file=adhell3-master.zip
SET adhell3_url=https://gitlab.com/fusionjack/adhell3/-/archive/master/%adhell3_file%
SET adhell3_folder=adhell3-master
SET app_folder=app
SET libs_folder=app\libs

SET knoxsdk_jar_file=knoxsdk.jar
SET supportlib_jar_file=supportlib.jar
SET app_properties_file=app.properties

SET param1=%1
SET param2=%2

IF /i [%param1%] == [clean] IF /i [%param2%] == [setup] GOTO clean_setup_adhell3
IF /i [%param1%] == [clean] IF /i [%param2%] == [build] GOTO clean_build_adhell3
IF /i [%param1%] == [setup] GOTO setup_adhell3
IF /i [%param1%] == [build] GOTO build_adhell3
GOTO error
goto:eof

:clean_setup_adhell3
echo Deleting %jdk8_folder% ...
del /f /s /q %jdk8_folder% 1>nul
rmdir /s /q %jdk8_folder%

echo Deleting %android_sdk_folder% ...
del /f /s /q %android_sdk_folder% 1>nul
rmdir /s /q %android_sdk_folder%

echo Deleting %tools_folder% ...
del /f /s /q %tools_folder% 1>nul
rmdir /s /q %tools_folder%

rem del %wget_file%
rem del %zip_file%
echo Deleting %jdk8_wildcard_file% ...
del %jdk8_wildcard_file%

echo Deleting %android_sdk_wildcard_file% ...
del %android_sdk_wildcard_file%
goto:setup_adhell3

:setup_adhell3
IF NOT EXIST "%wget_file%" (
  echo Downloading %wget_file% ...
  rem bitsadmin /transfer "wget" /download /priority normal %wget_url% "%cd%\%wget_file%"
  rem powershell -command "Invoke-WebRequest -Uri %wget_url% -OutFile %cd%\%wget_file%"
  powershell -command "(New-Object System.Net.WebClient).DownloadFile(\"%wget_url%\", \"%cd%\%wget_file%\")"
) ELSE (
  echo Found %wget_file%
)

IF NOT EXIST "%zip_file%" (
  echo Downloading %zip_file% ...
  wget %zip_url%
) ELSE (
  echo Found %zip_file%
)

IF NOT EXIST "%jdk8_wildcard_file%" (
  echo Downloading %jdk8_file% ...
  wget %jdk8_url%
) ELSE (
  echo Found %jdk8_file%
)

IF NOT EXIST %jdk8_folder% (
  echo Extracting %jdk8_file% ...
  7za x %jdk8_file%
)

IF NOT EXIST "%android_sdk_wildcard_file%" (
  echo Downloading %android_sdk_file% ...
  wget %android_sdk_url%
) ELSE (
  echo Found %android_sdk_file%
)

IF NOT EXIST %tools_folder% (
  echo Extracting %android_sdk_file% ...
  7za x %android_sdk_file%
)

IF NOT EXIST %android_sdk_folder% (
  echo Configuring Android SDK ...
  mkdir %android_sdk_folder%
  SET JAVA_HOME=%cd%\%jdk8_folder%\jre
  %tools_folder%\bin\sdkmanager "platform-tools" --sdk_root=%cd%\%android_sdk_folder%
)
goto:eof

:clean_build_adhell3
echo Cleaning build ...

IF EXIST "%adhell3_file%" (
  echo Deleting %adhell3_file% ...
  del %adhell3_file%
)

IF EXIST %adhell3_folder% (
  echo Deleting %adhell3_folder% folder ...
  del /f /s /q %adhell3_folder% 1>nul
  rmdir /s /q %adhell3_folder%
)

echo Getting latest adhell3 source code from gitlab ...
wget %adhell3_url%

echo Extracting %adhell3_file% ...
7za x %adhell3_file%
goto:build_adhell3

:build_adhell3
IF NOT EXIST %jdk8_folder% (
  echo Missing "%jdk8_folder%" folder, please run "adhell3 setup"
  exit /b
)

IF NOT EXIST %android_sdk_folder% (
  echo Missing "%android_sdk_folder%" folder, please run "adhell3 setup"
  exit /b
)

IF NOT EXIST "%knoxsdk_jar_file%" (
  echo Missing "%knoxsdk_jar_file%" file, please get it from Samsung SEAP and put it in the same folder where this script is located
  exit /b
)

IF NOT EXIST "%supportlib_jar_file%" (
  echo Missing "%supportlib_jar_file%" file, please get it from Samsung SEAP and put it in the same folder where this script is located
  exit /b
)

IF NOT EXIST "%app_properties_file%" (
  echo Missing "%app_properties_file%" file, please create it, set your application id and put it in the same folder where this script is located
  exit /b
)

IF NOT EXIST %adhell3_folder% (
  echo Getting latest adhell3 source code from gitlab ...
  wget %adhell3_url%

  echo Extracting %adhell3_file% ...
  7za x %adhell3_file%
)

SET JAVA_HOME=%cd%\%jdk8_folder%\jre
SET ANDROID_HOME=%cd%\%android_sdk_folder%

echo Building apk ...
cd %adhell3_folder%
copy ..\%app_properties_file% %app_folder%
IF NOT EXIST %libs_folder% (
  mkdir %libs_folder%
)
copy ..\%knoxsdk_jar_file% %libs_folder%
copy ..\%supportlib_jar_file% %libs_folder%
gradlew clean assembleDebug --no-daemon
cd ..
goto:eof

:error
echo Unknown parameter