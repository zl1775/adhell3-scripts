#!/bin/sh

param1=$1
param2=$2

jdk8_folder=jdk8u202-b08
jdk8_file=OpenJDK8U-jdk_x64_linux_hotspot_8u202b08.tar.gz
jdk8_wildcard_file=OpenJDK8U-jdk_x64_linux_hotspot_*.tar.gz
jdk8_url=https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/$jdk8_folder/$jdk8_file

android_sdk_folder=android-sdk
android_sdk_file=sdk-tools-linux-4333796.zip
android_sdk_wildcard_file=sdk-tools-linux-*.zip
android_sdk_url=https://dl.google.com/android/repository/$android_sdk_file
tools_folder=tools

adhell3_file=adhell3-master.zip
adhell3_url=https://gitlab.com/fusionjack/adhell3/-/archive/master/$adhell3_file
adhell3_folder=adhell3-master
app_folder=app
libs_folder=app/libs

knoxsdk_jar_file=knoxsdk.jar
supportlib_jar_file=supportlib.jar
app_properties_file=app.properties

if [ "$1" == "setup" ] || ([ "$1" == "clean" ] && [ "$2" == "setup" ]); then
   if [ "$1" == "clean" ]; then
      echo "Deleting $jdk8_folder ..."
      rm -rf $jdk8_folder

      echo "Deleting $android_sdk_folder ..."
      rm -rf $android_sdk_folder

      echo "Deleting $tools_folder ..."
      rm -rf $tools_folder

      echo "Deleting $jdk8_wildcard_file ..."
      rm $jdk8_wildcard_file

      echo "Deleting $android_sdk_wildcard_file ..."
      rm $android_sdk_wildcard_file
   fi

   if [ ! -f $jdk8_wildcard_file ]; then
      echo "Downloading $jdk8_file ..."
      wget $jdk8_url
   else
      echo "Found $jdk8_file"
   fi

   if [ ! -d $jdk8_folder ]; then
      echo "Extracting $jdk8_file ..."
      tar xvf $jdk8_file
   fi

   if [ ! -f $android_sdk_wildcard_file ]; then
      echo "Downloading $android_sdk_file ..."
      wget $android_sdk_url
   else
      echo "Found $android_sdk_file"
   fi

   if [ ! -d $tools_folder ]; then
      echo "Extracting $android_sdk_file ..."
      unzip $android_sdk_file
   fi

   if [ ! -d $android_sdk_folder ]; then
      echo "Configuring Android SDK ..."
      mkdir $android_sdk_folder
      export JAVA_HOME=$PWD/$jdk8_folder/
      ./$tools_folder/bin/sdkmanager "platform-tools" --sdk_root=$PWD/$android_sdk_folder
   fi

elif [ "$1" == "build" ] || ([ "$1" == "clean" ] && [ "$2" == "build" ]); then
   if [ "$1" == "clean" ]; then
      if [ -f $adhell3_file ]; then
         echo "Deleting $adhell3_file ..."
         rm $adhell3_file
      fi

      if [ -d $adhell3_folder ]; then
         echo "Deleting $adhell3_folder folder ..."
         rm -rf $adhell3_folder
      fi

      echo "Getting latest adhell3 source code from gitlab ..."
      wget $adhell3_url

      echo "Extracting $adhell3_file ..."
      unzip $adhell3_file
   fi

   if [ ! -d $jdk8_folder ]; then
      echo "Missing $jdk8_folder folder, please run 'bash adhell3.sh setup'"
      exit 1
   fi

   if [ ! -d $android_sdk_folder ]; then
      echo "Missing $android_sdk_folder folder, please run 'bash adhell3.sh setup'"
      exit 1
   fi

   if [ ! -f $knoxsdk_jar_file ]; then
      echo "Missing $knoxsdk_jar_file file, please get it from Samsung SEAP and put it in the same folder where this script is located"
      exit 1
   fi

   if [ ! -f $supportlib_jar_file ]; then
      echo "Missing $supportlib_jar_file file, please get it from Samsung SEAP and put it in the same folder where this script is located"
      exit 1
   fi

   if [ ! -f $app_properties_file ]; then
      echo "Missing $app_properties_file file, please create it, set your application id and put it in the same folder where this script is located"
      exit 1
   fi

   if [ ! -d $adhell3_folder ]; then
      echo "Getting latest adhell3 source code from gitlab ..."
      wget $adhell3_url

      echo "Extracting $adhell3_file ..."
      unzip $adhell3_file
   fi

   export JAVA_HOME=$PWD/$jdk8_folder/
   export ANDROID_HOME=$PWD/$android_sdk_folder

   echo "Building apk ..."
   cd $adhell3_folder
   cp ../$app_properties_file $app_folder/$app_properties_file
   
   if [ ! -d $libs_folder ]; then
      mkdir $libs_folder
   fi

   cp ../$knoxsdk_jar_file $libs_folder/$knoxsdk_jar_file
   cp ../$supportlib_jar_file $libs_folder/$supportlib_jar_file

   chmod +x gradlew
   ./gradlew clean assembleDebug --no-daemon

else
   echo "Unknown parameter"
fi
